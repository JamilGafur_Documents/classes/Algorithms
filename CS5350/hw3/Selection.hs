module Selection where

import System.Random
import Data.List (partition)
import Data.Array

selection :: [Int] -> Int -> IO Int
selection xs k = do
  p <- randomRIO (1, length xs)
  let atP = xs !! (p - 1)
  let (l1, l2) = partition (<= atP) xs
  
  case (length l1) `compare` k of
    EQ -> return atP
    GT -> selection l1 k
    LT -> selection l2 (k - (length l1))
