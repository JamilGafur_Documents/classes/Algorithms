\documentclass[11pt, a4paper]{article}
\usepackage{url}
% DOCUMENT LAYOUT
\usepackage[margin=1in]{geometry}

% Reduce spacing in enumerate, enumerate and titles
\usepackage{multicol}
\usepackage{enumitem}
\setenumerate{nolistsep}
\setlength\parindent{0pt}

\setlength{\parskip}{5pt}

% FONTS
\usepackage[utf8]{inputenc}

% HEADINGS and TITLES
\usepackage{sectsty}
\usepackage[normalem]{ulem}

% packages
\usepackage{amsmath, amsfonts, latexsym, listings, cite, amssymb,
  amsthm, stmaryrd, scrextend, float, mathtools}

\usepackage{graphicx}
\graphicspath{ {./img/} }

% Not native to tex
\usepackage{bussproofs}
\usepackage{qtree}
\usepackage{tikz-cd}
\usepackage{tikz}

\usepackage{titling}
\setlength{\droptitle}{-90pt}
\title{CS5350: Homework 3}
\author{Alex Hubers, Jamil Gafur, Xiang Liu}

% Thm styling if I need it
\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]
\newtheorem{theorem}{Theorem}[section]
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem*{remark}{Remark}

% pretty references
\usepackage{prettyref}
\newcommand{\pref}[1]{\prettyref{#1}}
\newrefformat{assn}{Assumption~\ref{#1}}
\newrefformat{prop}{Property~\ref{#1}}
\newrefformat{fig}{Figure~\ref{#1}}
\newrefformat{sec}{Section~\ref{#1}}
\newrefformat{thm}{Theorem~\ref{#1}}
\newrefformat{lem}{Lemma~\ref{#1}}
\newcommand\secref[1]{\pref{sec:#1}}
\newcommand\figref[1]{\pref{fig:#1}}
\newcommand\appref[1]{\pref{app:#1}}

\bibliographystyle{acm}

\usepackage{listings}
\lstloadlanguages{Haskell}
% \usepackage{libertine}
\usepackage{lmodern}
\lstset{
  showstringspaces=false,
  language=Haskell,
  numbers=left,
  xleftmargin=\parindent,
  basicstyle=\ttfamily,
  % keepspaces=false,
  morecomment=[l]{--},
  mathescape=true,
  numberstyle=\tiny,
  flexiblecolumns=false,
  basewidth={0.5em,0.45em},
  morekeywords={instance, Theorem, Lemma, class, where, if, data, then, else, type, case, of, require, newtype},
  keywordstyle={\bfseries},%\underbar,
}
\lstnewenvironment{code}{}{}
%\lstMakeShortInline{|}
\lstnewenvironment{codef}{\lstset{basicstyle=\ttfamily\small,xleftmargin=1.5em,language=python}}{}
\lstnewenvironment{codefHS}{\lstset{basicstyle=\ttfamily\small,xleftmargin=1.5em,language=python}}{}

% My Commands
\newcommand{\LA}{\ensuremath{\langle}}
\newcommand{\RA}{\ensuremath{\rangle}}
\newcommand{\binset}{\ensuremath{\lbrace 0, 1 \rbrace}}
\newcommand{\Prob}[1]{\ensuremath{\mathbb{P}[#1]}}
\newcommand{\Ln}[1]{\ensuremath{ln(#1)}}

% BEGIN DOCUMENT
\begin{document}
\maketitle

\section*{Exercise 1}
As mentioned in class, there is a simple randomized Las Vegas algorithm
that solves the \verb!SELECTION! problem, defined below.

\begin{codef}
SELECTION
Input: A list $L$ of length $n, k \in \{ 1, 2, ..., n \}$.
Output: The rank-$k$ element in $L$.
\end{codef}

Note that an element in a list is said to have \emph{rank k} if it is the $k$-th
smallest element in the list.

The algorithm for \verb!SELECTION! that I am thinking of is very similar to the randomized
Quicksort algorithm discussed in class. An informal description of this
algorithm is as follows.  We pick a \emph{pivot} index $p \in \{1, 2, ..., n \}$
uniformly at random and construct sublists
$L_{1} = \{ u \in L\; |\; u \leq L[p] \}$ and
$L_{2} = \{ v \in L\; |\; v > L[P]\}$.  If $L[p]$ has rank $k$, we are done.
Otherwise, we recurse on one of $L_{1}$ \emph{or} $L_{2}$ looking for an element
of an appropriate rank.

\begin{enumerate}[label=(\alph*)]
\item State this algorithm precisely in pseudocode.
\item Show that the expected running time of the algorithm is $O(n)$ by
  mimicking the approach used for analyzing randomized Quicksort in
  class. Specifically, let $X_{ij}$ denote the number of times the rank-$i$
  element and the rank-$j$ element are compared by the algorithm. Let
  $X = \sum_{i}\sum_{j} X_{ij}$ denote the total number of comparisons made by
  the algorithm.  Show that $E[X] = O(n)$.
\end{enumerate}

\subsection*{Solution (a)}

In roughly Haskell pseudo-notation. Assume $k \leq n$.

\begin{codefHS}
selection :: Array Int $\to$ Int $\to$ IO Int
selection L[1..n] k = do
  p $\leftarrow$ random (1, n)
  let 
    atP = $L$ !! p                                  
    (L$_{1}$, L$_{2}$) = partition ($\leq$ atP) L     
  
  case (length L$_{1}$) `compare` k of             
    EQ -> return atP
    GT -> selection L$_{1}$ k
    LT -> selection L$_{2}$ (k - (length L$_{1}$))
\end{codefHS}

\subsection*{Solution (b)}
First, let's look at the algorithm: assume constant cost of finding a random $p$
(line 3); a constant cost for \verb!Array! access (line 5); a linear cost of
partitioning (line 6); and a constant cost of accessing \verb!Array length! (line 8). It is
safe, then, to conclude that we may just count the comparisons incurred by
\verb!partition! in order to measure the cost of this algorithm.


The start of our proof proceeds the same as for Quicksort. Let $X_{ij}$
be an indicator variable denoting if the rank-$i$ element is compared
to the rank-$j$ element.

$$
X_{ij} =
\begin{cases}
  1 \quad \text{ if } a_{i} \text { compared with } a_{j} \\
  0 \quad \text{otherwise}
\end{cases}
$$

We are justified in letting this be an indicator variable because it is not
possible that two elements of $L$ are ever compared more than once (and therefore, $X_{ij} \in \{0, 1 \}$). The
argument is the same as for Quicksort: if
$a_{1}, ..., a_{i}, ..., a_{j}, ..., a_{n}$ denote the sorted elements of $L$ and
$S = \{a_{i}, ..., a_{j} \}$, then $a_{i}$ is compared with $a_{j}$ iff the
first chosen pivot from $S$ is either $a_{i}$ or $a_{j}$, and they are never compared
thereafter.

We want to determine $E[X]$ by first figuring out $E[X_{ij}]$. With Quicksort,
we observed that the $X_{ij} = 1$ if the first pivot chosen from $S$ is either $a_{i}$
or $a_{j}$, and therefore (for Quicksort only),

$$
E[X_{ij}] = \frac{2}{j - i + 1}
$$

i.e, there are 2 ``good'' slots in the range $(i, j)$ to pick from. With
\verb!selection!, we only search along one partition, depending on the pivot's relation
to $k$. Consequently, we have different bounds to our range. For example, if
$a_{k}$ (the rank-$k$ element) comes before $a_{i}$ and we pick a pivot $a_{u}$
such that $a_{k} < a_{u} < a_{i} < a_{j}$, then we will never compare $a_{i}$
and $a_{j}$. Both will be thrown into $L_{2}$ while we recurse upon $L_{1}$,
which contains $a_{k}$.


So let's refine our argument from before, and ask where is $a_{k}$? It's either
before $a_{i}$, in between $a_{i}$ and $a_{j}$, or after $a_{j}$ (or, of course,
it equals $a_{i}$ or $a_{j}$; we'll roll those posibilities in). We will show
that in all three cases, $E[X] \in O(n)$.

\paragraph{Case: $i < j \leq k$.} The no-no zone is between
$a_{i}$ and $a_{k}$. So:

$$
E[X_{ij}] = \frac{2}{k - i + 1}
$$

and

\begin{align*}
  \phantom{} & E[X] \\
  = & \sum_{i = 1}^{k - 1}\sum_{j = i + 1}^{k} \frac{2}{k - i + 1} & \\
  =\, & 2 \sum_{i = 1}^{k - 1} \frac{k - i}{k - i + 1} \quad &(j \text{ isn't in term, and } k - i \text{ iterations in } \sum_{j = i + 1}^{k}) \\
  \leq\, & 2 \sum_{i = 1}^{k - 1} 1 \quad &(\text{Since } \frac{k - i}{k - i + 1} \leq 1) \\
  =\, & 2 (k - 1) \quad &(k - 1 \text{ iterations}) \\
  \leq\, & 2n \quad &(k - 1 \leq n) \\
\end{align*}


\paragraph{Case: $k \leq i < j$.} This one is more or less symmetric to the previous one.
Our no-no zone is between $a_{k}$ and $a_{j}$, so

$$
E[X_{ij}] = \frac{2}{j - k + 1}
$$

and

\begin{align*}
  \phantom{} & E[X] \\
  =\, &\sum_{i = k}^{j - 1}\sum_{j = k + 1}^{n} \frac{2}{j - k + 1} & \\
  =\, &\sum_{j = k + 1}^{n}\sum_{i = k}^{j - 1} \frac{2}{j - k + 1} \quad & (\text{flippy floppy}) \\
  =\,&2 \sum_{j = k + 1}^{n} \frac{j - k}{j - k + 1} \quad & (\text{(j - k) iterations in } \sum_{i = k}^{j - 1}) \\
  \leq\, &2n \quad & (\text{identical argument as above})  
\end{align*}
%   =\, & \sum_{i = k + 1}^{n}\sum_{j = k}^^{n} \frac{2}{j - k + 1} \\



\paragraph{Case: $i < k < j$.} This one is a doozy, so bear with me. We have

$$
E[X_{ij}] = \frac{2}{j - i + 1}
$$

and now

\begin{align*}
  \phantom{} & E[X] \\
  = & \sum_{i = 1}^{k - 1}\sum_{j = k + 1}^{n} \frac{2}{j - i + 1} & \\
  =\, & 2 \sum_{j = k + 1}^{n} \left[ \frac{1}{j} + \frac{1}{j - 1} + ... + \frac{1}{j - k + 2} \right]   \quad &(\text{unrolling the outer } \Sigma) \\
  =\, & 2 \left[ \sum_{j = k + 1}^{n} \frac{1}{j} + \sum_{j = k + 1}^{n} \frac{1}{j - 1} + ... + \sum_{j = k + 1}^{n} \frac{1}{j - k + 2} \right]   \quad &(\text{pushing the } \Sigma \text{ in}) \\
  =\, & 2 \left[ (\sum_{1}^{n} \frac{1}{j} - \sum_{1}^{k} \frac{1}{n} ) + (\sum_{1}^{n - 1} \frac{1}{j} - \sum_{1}^{k - 1} \frac{1}{j} ) + ... + (\sum_{1}^{n - k + 2} \frac{1}{j} - (1 + \frac{1}{2})) \right] \quad &(\text{reindexing sums})
\end{align*}

Now observe that these are harmonic series, i.e,

$$
H_{n} = \sum_{1}^{n} \frac{1}{n} = ln(n) + \gamma \leq ln(n) + 1
$$

Below there isn't enough space for annotations, but in gist we: replace sums
with harmonic series; cancel out the 1's; rewrite the logarithms into one 'biggun; turn
this thingy into a combination; and show the combination is less than an
exponential.

\small{
\begin{align*}
  =\, & 2 \left[ (\sum_{1}^{n} \frac{1}{j} - \sum_{1}^{k} \frac{1}{j} ) + (\sum_{1}^{n - 1} \frac{1}{j} - \sum_{1}^{k - 1} \frac{1}{j} ) + ... + (\sum_{1}^{n - k + 2} \frac{1}{j} - (1 + \frac{1}{2})) \right] \\
  =\, & 2 \left[ (H_{n} - H_{k}) + (H_{n - 1} - H_{k - 1}) + ... + (H_{n - k + 2} - H_{2}) \right] & \\
  \leq\, & 2 \left[ ((\Ln{n} + 1) - (\Ln{k} + 1)) + ((\Ln{n - 1} + 1) - (\Ln{k - 1} + 1)) + ... + ((\Ln{n - k + 2} + 1) + (\Ln{2} + 1)) \right] & \\
  =\, & 2 \left[ (\Ln{n} - \Ln{k}) + (\Ln{n - 1} - \Ln{k - 1}) + ... + (\Ln{n - k + 2} + \Ln{2}) \right] & \\
  =\, & 2 \left[ \Ln{\frac{n}{k}} + \Ln{\frac{n -1}{k - 1}} + .... + \Ln{\frac{n - k + 2}{2}} \right] & \\
  =\, & 2 \left[ \Ln{\frac{n (n - 1) ... (n - k + 2)}{k!}} \right] & \\
  =\, & 2 \left[ \Ln{\frac{n!}{(n - k + 2)!k!}} \right] & \\
  \leq\, & 2 \left[ \Ln{\frac{n!}{(n - k)!k!}} \right] & \\
  =\, & 2 \left[ ln\, {n \choose k} \right] & \\
  \leq\, & 2\, \Ln{2^{n}} & \\
  \leq\, & 2n\, \Ln{2}
\end{align*}

and $2n\, \Ln{2} \in O(n)$.

\section*{Exercise 2}

Suppose we throw $n$ balls into $n$ bins one by one. For each ball, a bin is
chosen uniformly at random from the $n$ bins, and the ball is thrown into
the chosen bin. The choice of a bin for a particular ball is independent of all
other bin choices. Use Chernoff bounds and the union bound to show that for
some constant $c$, the maximum number of balls in any bin is at most $c\; log(n)$
with probability at least $1-\frac{1}{n-1}$.

\subsection*{Solution}

Consider an arbitrary ball $i$ and bin $m$, let $X_{mi}$ be an indicator variable such that
$$
X_{mi} =
\begin{cases}
  1 \quad \text{if ball } i \text{ goes into bin } m \\
  0 \quad \text{otherwise}
\end{cases}
$$

and let $X_{m} = \sum_{i} X_{mi}$. Each ball $i$ has a uniformly random chance of being
in bin $m$ over $n$ throws, so the probability that ball $i$ is in bin $m$ is
given by

$$
\Prob{X_{mi} = 1} =  E[X_{mi}] = 1/n
$$


and the expected value $E[X_{m}]$ is given by

$$
E[X_{m}] = \mu = \sum_{i}^{n} E[X_{mi}] = \sum_{i}^{n} \frac{1}{n} = 1
$$

We want to calculate the probability that any given bin $X_{m}$ has more than
$c\, log(n)$ for some constant $c$, or,

$$
\Prob{X_{m} \geq c\, log(n)}
$$

As $\mu = E[X_{m}] = 1$, we have Chernoff bounds:

$$
\Prob{ X  \geq 1 +  (c\, log(n) - 1)} \leq e^{-\frac{(c\, log(n) - 1)^{2}}{c\, log(n) + 1}}
$$

The probability that \emph{any} of the $n$ bins has more than $c\, log(n)$ balls is given by
the union bound. Let $\Delta$ denote $c\, log(n)$.

$$
  \Prob{ X_{1} \geq \Delta \vee X_{2} \geq \Delta \vee ... \vee X_{n} \geq \Delta }
\leq \Prob{ X_{1} \geq \Delta } + \Prob{X_{2} \geq \Delta} + ... + \Prob{X_{n} \geq \Delta }
= n \cdot e^{-\frac{(c\, log(n) - 1)^{2}}{c\, log(n) + 1}}
$$

We want the probability of this to be $\leq 1/n$, which will happen if

$$
e^{-\frac{(c\, log(n) - 1)^{2}}{c\, log(n) + 1}} \leq \frac{1}{n^{2}}
$$

Now let's do some cleanup and simplification. Let $\Delta$ again denote
$c\, log(n)$. Observe that $f(x) = e^{-x}$ is antitone: if $x \leq y$ then
$e^{-x} \geq e^{-y}$.

\begin{align*}
  \phantom{} & \frac{(\Delta - 1)^{2}}{\Delta - 1} \leq \frac{(\Delta - 1)^{2}}{\Delta + 1} \\
  \Rightarrow\, &\Delta - 1 \leq \frac{(\Delta - 1)^{2}}{\Delta + 1} \\
  \Rightarrow\, &e^{-\frac{(\Delta - 1)^{2}}{\Delta + 1}} \leq e^{- (\Delta - 1)}  \\
  \Rightarrow\, &e^{-\frac{(\Delta - 1)^{2}}{\Delta + 1}} \leq e^{-c\, log(n)}e  \\
  \Rightarrow\, &e^{-\frac{(\Delta - 1)^{2}}{\Delta + 1}} \leq \frac{e}{n^{c}} \\
\end{align*}


And now we want to choose $c$ such that:

$$
e^{-\frac{(c\, log(n) - 1)^{2}}{c\, log(n) + 1}} \leq \frac{e}{n^{c}} \leq \frac{1}{n^{2}}
$$

Note we only care for when $n \geq 2$; if $n = 1$, then we know exactly how many
balls (one) will be landing into one bins. $c \geq 4$ should work.

\section*{Exercise 3}
There are $n$ client machines, some of which are colored \verb!Red! and the rest are colored
\verb!Blue!. You know that there are more \verb!Red! clients than there are \verb!Blue! clients.
A server machine, which knows the number $n$, wants to find out how many clients are colored
\verb!Red!. Directly communicating with each client is costly and so the server communicates
with $T \ll n$ randomly chosen clients to find out their colors. The server then uses
what it knows about the colors of these $T$ (probed) clients to \emph{estimate} the total number
of \verb!Red! clients. Specifically, if $r$ of the $T$ clients that the server probed are
\verb!Red! then the server estimates that $n \cdot r/T$ of the total clients are red.

How many clients $T$ should the server probe in order to obtain an estimate that is within
$(1 \pm \epsilon)$ times the actual number of \verb!Red! clients, with probability
at least $1 - 1/n$? Justify your answer using Chernoff bounds.

\subsection*{Solution}
Let indicator variable $X_{i}$ be 1 when the $i$th measured client is red and 0
otherwise. Each $X_{i}$ is independent (we're just sampling). Assume that there
are $k$ actual red clients out of the total $n$. Then

$$
\Prob{X_{i} = 1} = E[X_{i}] = \frac{k}{n}
$$

As usual, let $X = \sum_{i}^{T} X_{i}$. So $X$ indicates how many red clients we
observed. By linearity of expectation,

$$
E[X] = \sum_{i}^{T} E[X_{i}] = \frac{k T}{n}
$$

and we get Chernoff bounds

\begin{gather*}
\Prob{X \geq (1 + \epsilon)\frac{kt}{n}} \leq e^{-\frac{\epsilon^{2} kt}{3n}} \\
\Prob{X \leq (1 - \epsilon)\frac{kt}{n}} \leq e^{-\frac{\epsilon^{2} kt}{2n}}
\end{gather*}

Supposing the server measures $X$ red clients and then estimates the total to be
$n \cdot X/T$, we wish to show that this estimate is within $(1 \pm \epsilon)$
times the actual number of red clients, $k$. In other words, we want the probability that

$$
\frac{X n}{T} \geq (1 + \epsilon)k
$$

plus the probability that

$$
\frac{X n}{T} \leq (1 - \epsilon)k
$$

But multiplying both sides by $\frac{T}{n}$ yields the desired Chernoff bounds above, i.e,

\begin{gather*}
  \Prob{\frac{X n}{T} \geq (1 + \epsilon)k} = \Prob{X \geq (1 + \epsilon)\frac{kT}{n}} = e^{-\frac{\epsilon^{2} kT}{3n}} \\
  \Prob{\frac{X n}{T} \leq (1 - \epsilon)k} = \Prob{X \leq (1 - \epsilon)\frac{kT}{n}} = e^{-\frac{\epsilon^{2} kT}{2n}}
\end{gather*}

Summing these two gives

$$
e^{-\frac{\epsilon^{2} kT}{3n}} + e^{-\frac{\epsilon^{2} kT}{2n}}
$$

which tells us the probability that $X$ is at the tails. To make our analysis
easier, we would prefer to reduce this to just one of the two terms times some
constant. Since $\frac{\epsilon^{2} kT}{3n} \leq \frac{\epsilon^{2} kT}{2n}$, we have
$e^{-\frac{\epsilon^{2} kT}{2n}} \leq e^{-\frac{\epsilon^{2} kT}{3n}}$, and consequently,

$$
e^{-\frac{\epsilon^{2} kT}{3n}} + e^{-\frac{\epsilon^{2} kT}{2n}} \leq 2e^{-\frac{\epsilon^{2} kT}{3n}}
$$

We want to know the probability that $X$ is within $\epsilon$ range of our estimate,
which is given by the complement,

$$
1 - 2e^{-\frac{\epsilon^{2} kT}{3n}}
$$

By assumption, more than half are red. So we can do some simplification
by swapping out $\frac{k}{n}$ for $1/2$. To give the derivation precisely:

\begin{align*}
  \phantom{} &\frac{1}{2} \leq \frac{k}{n} \\
  \Rightarrow &\frac{\epsilon^{2}T}{6} \leq \frac{\epsilon^{2}kT}{3n} \quad \text{(Multiply by } \epsilon^{2}T/3) \\
  \Rightarrow &e^{-\frac{\epsilon^{2}T}{6}} \geq e^{-\frac{\epsilon^{2}kT}{3n}} \quad \text{(Antitonicity of } e^{-x}) \\
  \Rightarrow &-2e^{-\frac{\epsilon^{2}T}{6}} \leq -2e^{-\frac{\epsilon^{2}kT}{3n}} \quad \text{(Antitonicity of } f(x) = -2x) \\
  \Rightarrow & 1 - 2e^{-\frac{\epsilon^{2}kT}{3n}} \geq  1 - 2e^{-\frac{\epsilon^{2}T}{6}}  \quad \text{(Swap sides and add } 1) \\
\end{align*}

And now we solve for $T$ with the desired probability at least $1 - 1/n$.

\begin{align*}
  \phantom{} &1 - 2e^{-\frac{\epsilon^{2}kT}{3n}} \geq 1 - 2e^{-\frac{\epsilon^{2} T}{6}} \geq 1 - \frac{1}{n} \quad \text{(Showing transitivity in argument}) \\
  \Rightarrow\, &2e^{-\frac{\epsilon^{2} T}{6}} \leq \frac{1}{n} \quad (\text{Subtract 1}) \\
  \Rightarrow\, &e^{-\frac{\epsilon^{2} T}{6}} \leq \frac{1}{2n} \quad (\text{Divide by two}) \\
  \Rightarrow\, &-\frac{\epsilon^{2} T}{6} \leq ln(\frac{1}{2n}) \quad (\text{Take log}) \\
  \Rightarrow\, &-\frac{\epsilon^{2} T}{6} \leq ln(2^{-1}n^{-1}) \quad (\text{Simplify log}) \\
  \Rightarrow\, &-\frac{\epsilon^{2} T}{6} \leq -ln(2) - ln(n) \quad (\text{Simplify log}) \\
  \Rightarrow\, &T \geq \frac{6}{\epsilon^{2}} \cdot (ln(2) + ln(n))  \quad (\text{Multiply by } \frac{-6}{\epsilon^{2}}) \\
\end{align*}

So choosing $T$ greater than $\frac{6}{\epsilon^{2}} \cdot (ln(2) + ln(n))$ yields an estimate within $\epsilon$ of the desired
probability. Note that $T \ll n$: $T$ need only be $O(log(n))$.

\section*{Exercise 4}
We are given a list $L$ with $n$ elements and we want to find the two largest elements
in $L$. Here is a simple algorithm for the problem.

\begin{codef}
max2(L[1..n]):
  m1 = L[1]
  m2 = L[2]
  if m1 < m2 then swap(m1, m2)

  for i = 3 to n do
    if L[i] $\leq$ m2 then
      break
    else m2 = L[i]
    if m1 < m2 then swap(m1, m2)
  return m1, m2
\end{codef}

In the worst case, this algorithm uses $1 + 2(n - 2) = 2n - 3$ comparisons.
The number of comparisons can be reduced by the use of randomization. Specifically,
show that if we randomly permute $L$ before calling \verb!max2!, then the expected
number of comparisons is $n + O(log\; n)$.

\subsection{Solution}
Let indicator variable $X_{i}$ be given as

$$
X_{i} =
\begin{cases}
  0 \quad L[i] \leq m_{2} \\
  1 \quad L[i] > m_{2}
\end{cases}
$$

And let $X = \sum_{i = 3}^{n} X_{i}$. Observe that we will always perform at least one
comparison (line 7 of algorithm) per iteration. We will perform 2 precisely when
$L[i] > m_{2}$, or $X_{i} = 1$. So the number of comparisons is given by

$$
1 + (n - 2) + X = n - 1 + X
$$

We now estimate $X$. Our analysis will proceed by considering an arbitrary step
$3 \leq i \leq n$ in our computation. The probability $\Prob{X_{i} = 1}$ is
given by the probability that $L[i] > m_{2}$. Conceptually, it helps to pretend
that we have sorted the sublist $L[1..i-1]$. Now pick a random element from $L$;
what is the probability that it exceeds $L[i-1]$ and $L[i - 2]$? Because the
list is randomly permuted beforehand, we have an equal probability to be
``inserted'' at any position in $L[1]$ through $L[i - 1]$. Of the $i$ slots,
$L[i]$ exceeds $m_{2}$ if it is to be inserted after $L[i - 2]$ or
$L[i -1]$\footnote{This is not the best way to convey this thought. We
  definitely know what we're trying to express, but not how to express it
  formally.}. So

$$
\Prob{X_{i} = 1} = \frac{2}{i}
$$

Then

$$
E[X] = \sum_{i = 3}^{n} \frac{2}{i}
$$

This is a harmonic sum, which grows logarithmically.  So $E[X] \in O(log\, n)$ and
the expected number of comparisons is $n - 1 + O(log\, n)$.

% \section{Acknowledgements}
% \begin{itemize}
% \item For problem 2, we used
%   \url{https://pages.cs.wisc.edu/~shuchi/courses/787-F09/scribe-notes/lec7.pdf}
%   to inform our analysis. In particular, it helped find the probability that the
%   amount of balls in a particular bin is at least some $k$.
% \end{itemize}
%
\end{document}
