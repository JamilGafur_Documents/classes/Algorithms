module GreedyPartition where

greedy :: [Int] -> (Int, Int)
greedy xs = go xs 0 0
  where
    go (x : xs) a b
      | a < b     = go xs (a + x) b
      | otherwise = go xs  a     (b + x)
    go [] a b = (a, b)

-- >>> greedy [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
-- (30,25)

-- >>> greedy [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
-- (27,28)
