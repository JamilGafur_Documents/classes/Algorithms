\documentclass[11pt, a4paper]{article}
\usepackage{url}
% DOCUMENT LAYOUT
\usepackage[margin=1in]{geometry}

% Reduce spacing in enumerate, enumerate and titles
\usepackage{multicol}
\usepackage{enumitem}
\setenumerate{nolistsep}
\setlength\parindent{0pt}

\setlength{\parskip}{5pt}

% FONTS
\usepackage[utf8]{inputenc}

% HEADINGS and TITLES
\usepackage{sectsty}
\usepackage[normalem]{ulem}

% packages
\usepackage{amsmath, amsfonts, latexsym, listings, cite, amssymb,
  amsthm, stmaryrd, scrextend, float, mathtools}

\usepackage{graphicx}
\graphicspath{ {./img/} }

% Not native to tex
\usepackage{bussproofs}
\usepackage{qtree}
\usepackage{tikz-cd}
\usepackage{tikz}

\usepackage{titling}
\setlength{\droptitle}{-90pt}
\title{CS5350: Homework 4}
\author{Alex Hubers, Jamil Gafur, Xiang Liu}

% Thm styling if I need it
\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]
\newtheorem{theorem}{Theorem}[section]
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem*{remark}{Remark}

% pretty references
\usepackage{prettyref}
\newcommand{\pref}[1]{\prettyref{#1}}
\newrefformat{assn}{Assumption~\ref{#1}}
\newrefformat{prop}{Property~\ref{#1}}
\newrefformat{fig}{Figure~\ref{#1}}
\newrefformat{sec}{Section~\ref{#1}}
\newrefformat{thm}{Theorem~\ref{#1}}
\newrefformat{lem}{Lemma~\ref{#1}}
\newcommand\secref[1]{\pref{sec:#1}}
\newcommand\figref[1]{\pref{fig:#1}}
\newcommand\appref[1]{\pref{app:#1}}

\newcommand\varsequence[4]{\ensuremath{#1_{#2} #3... #3 #1_{#4}}}

\newcommand{\commentbox}[3]{{\par\noindent\small\color{#1} \framebox{\parbox{\dimexpr\linewidth-2\fboxsep-2\fboxrule}{\textbf{#2:} #3}}}}
\newcommand{\AH}[1]{\commentbox{black}{AH}{#1}}

\bibliographystyle{acm}

\usepackage{listings}
\lstloadlanguages{Haskell}
% \usepackage{libertine}
\usepackage{lmodern}
\lstset{
  showstringspaces=false,
  language=Haskell,
  numbers=left,
  xleftmargin=\parindent,
  basicstyle=\ttfamily,
  % keepspaces=false,
  morecomment=[l]{--},
  mathescape=true,
  numberstyle=\tiny,
  flexiblecolumns=false,
  basewidth={0.5em,0.45em},
  morekeywords={while, then, do, instance, Theorem, Lemma, class, where, if, data, then, else, type, case, of, require, newtype},
  keywordstyle={\textbf},%\underbar,
}
\lstnewenvironment{code}{}{}
%\lstMakeShortInline{|}
\lstnewenvironment{codef}{\lstset{basicstyle=\ttfamily\small,xleftmargin=1.5em,language=python}}{}
\lstnewenvironment{codefHS}{\lstset{basicstyle=\ttfamily\small,xleftmargin=1.5em,language=python}}{}

% My Commands
\newcommand{\LA}{\ensuremath{\langle}}
\newcommand{\RA}{\ensuremath{\rangle}}
\newcommand{\binset}{\ensuremath{\lbrace 0, 1 \rbrace}}
\newcommand{\Prob}[1]{\ensuremath{\mathbb{P}[#1]}}
\newcommand{\Ln}[1]{\ensuremath{ln(#1)}}
\newcommand{\ceiling}[1]{\lceil #1 \rceil}
\newcommand{\Log}[1]{\text{log } #1}

% BEGIN DOCUMENT
\begin{document}
\maketitle

\section*{Exercise 1}
Consider the execution of Luby's MIS algorithm on a cycle $C$ of length $n$.

\begin{enumerate}[label=(\alph*)]
\item Consider an arbitrary vertex $v$ that is active at the start of some iteration $i$. Prove that
  \[
    \Prob{v \text{ joins the MIS in iteration } i} \geq c
  \]
  for some constant $c > 0$.
\item Consider a path $P$ of vertices all of which are active at the start of some iteration
  $i$. Using the result in (a), show that the probability that all vertices in $P$ are active after
  iteration $i$ is at most $1/{2^{c'|P|}}$ for some constant $c' > 0$.
  
  \textbf{Note:} Be careful; if $u$ and $v$ are neighbors then the event that $u$ stays active
  after iteration $i$ is not independent of the event that $v$ stays active after iteration $i$. But,
  if you pick vertices that are far enough from each other in $P$, you can still use independence.
\item Consider a path $P$ of length $\ceiling{\sqrt{\log{n}}\,}$ in $C$. Show that
  for a sufficiently large constant $C$, the probability that all vertices in
  $P$ are active after $C\, \sqrt{\log{n}}$ iterations is at most
  $1/n^{2}$.
\item Modify Luby's MIS algorithm so that it terminates in $O(\sqrt{\log{n}})$
  rounds with high probability on cycles of length $n$. Use the result in (c) to
  guide your modification. Show that your algorithm is correct and that it has
  the desired running time.
\end{enumerate}

\newcommand\Deg[1]{\text{deg}(#1)}
\newcommand\Marked[1]{#1 \text{ marked}}

\subsection*{Solution (a)}

 $C$ is a cycle, so the degree of $v$
is either $2$ or $1$.  We are trying to find a lower bound on the probability
that $v$ joins the MIS, so we take the worse of outcomes. So we assume that

\begin{align*}
  \Deg{v} &= 2 \\
  \Deg{v_{l}} &= 1 \\
  \Deg{v_{r}} &= 1
\end{align*}

where $v$'s neighbors, $v_{l}$ and $v_{r}$, each have only $v$ as an active
neighbor (and are thus more likely to join the MIS than $v$). $v$ has higher degree than its neighbors, and so will only join the
MIS if (i) it is marked, and (ii) neither of its two neighbors are marked. This
means that

\begin{gather*}
  \Prob{v \text{ joins the MIS}} \geq \Prob{\Marked{v}} * \Prob{\neg(\Marked{v_{l}})}  *\Prob{\neg(\Marked{v_{r}})} \\
  = \frac{1}{4} * \frac{1}{2} * \frac{1}{2}
  = \frac{1}{2^{4}}
\end{gather*}

\begin{remark}
  For part (b), it's more helpful to consider a vertex which has two active
  neighbors (as $v$ is part of a path of all active nodes):

  \begin{gather*}
    \Prob{v \text{ joins the MIS}} = \Prob{\Marked{v}} * \Prob{\neg(\Marked{v_{l}})} * \Prob{\neg(\Marked{v_{r}})} * \Prob{v \text{ has highest id}} \\
    = \frac{1}{4} * (\frac{3}{4})^{2} * \frac{1}{3} \\
    = \frac{3}{64}
  \end{gather*}
\end{remark}

%% We can do a bit better than this approximation by accounting for the event that its neighbors are marked, i.e.,
%% 
%% \begin{align*}
%%   \Prob{ v \text{ joins the MIS}} = \Prob{&\Marked{v} \\
%%   &\wedge ((\Marked{v_{r}} \wedge v_{r}.id < v.id) \vee \neg(\Marked{v_{r}})) \\
%%   &\wedge ((\Marked{v_{l}} \wedge v_{l}.id < v.id) \vee \neg(\Marked{v_{l}})) } 
%% \end{align*}
%% 
%% i.e, $v$ will join the MIS if: $v$ is marked and its neighbors are not marked, or,
%% its neighbors are marked but have a lesser id. We are assuming ids are more or less assigned
%% arbitrarily, so the probability $v.id > v_{l}$ or $v.id > v_{r}$ is simply $1/2$. So this becomes
%% \begin{align*}
%%   &\phantom{} \frac{1}{4} * ((\frac{1}{4} * \frac{1}{2}) + \frac{3}{4})^{2} \\
%%   &= \frac{1}{2} * (\frac{1}{8} + \frac{3}{4}) \\
%%   &= \frac{1}{2} * \frac{7}{8} \\
%%   &= \frac{7}{16}
%% \end{align*}
%%

\newcommand{\Mod}[3]{#1 \equiv #2\, \mathrm{mod}\ #3}

\subsection*{Solution (b)}
Assume path $P$ of length $k$ starts at $p_{1}$ and each $p_{i}$ is neighbors to
$p_{i - 1}$ and $p_{i + 1}$, $i < 1 < k$. The probability that all vertices in
path $P$ are active at iteration $i + 1$ is bounded above by

\begin{gather*}
  \Prob{p_{1} \text{ doesn't join the MIS}} * ... * \Prob{p_{k} \text { doesn't join the MIS}}
\end{gather*}

The probability that arbitrary vertex $p_{i}$ joins the MIS is
dependent upon the probability that its neighbors join the MIS.
We can, instead, consider only every other vertex in $P$.

\[
  M = \{\; p_{i}\; \mid \; \Mod{i}{0}{2}\; \}
\]

It is sane to do so, as

\[
\Prob{\text{all } p \in P \text{ remain active}} \leq \Prob{\text{all } m \in M \text{ remain active}}
\]

and we're trying to find an upper bound. Taking the solution from (a), we get

\begin{gather*}
  \Prob{\text{all } p \in P \text{ remain active}} \\
  \leq \Prob{\text{all } m \in M \text{ remain active}} \\
  = \Prob{p_{2} \text{ doesn't join the MIS}} * \Prob{p_{4} \text{ doesn't join the MIS}} * ... * \Prob{p_{m} \text{ doesn't join the MIS}} \\
  = (1 - \frac{3}{4^{3}})^{\frac{k}{2}} \\
  \leq e^{-\frac{3k}{4^{3}*2}} \\
  \leq \frac{1}{2^{\frac{3k}{2^{7}}}}
\end{gather*}

\subsection*{Solution (c)}
The probability that every vertex in path $P$ of length $\sqrt{\log{n}}$ is
active after $C \sqrt{\log{n}}$ iterations is simply the probability that
\textbf{Solution (b)} happens for $C \sqrt{\log{n}}$ iterations.

\begin{gather*}
  \Prob{\text{every vertex in } P \text{ remains active after } C\sqrt{\log{n}} \text{ rounds}} \\
  \leq \Prob{\text{all } p \in P \text{ active after round } 1} * ... *
       \Prob{\text{all } p \in P \text{ active after round } C\sqrt{\log{n}}} \\
  = (\frac{1}{2^{\frac{3\sqrt{\log{n}}}{2^{7}}}})^{C \sqrt{\log{n}}} \\
  = \frac{1}{2^{\frac{3C \log{n}}{2^{7}}}} \\
  = \frac{1}{n^{\frac{3C}{2^{7}}}}
\end{gather*}

Solving for $\frac{3C}{2^{7}} = 2$ gives $C = 2^{8}/3 \approx 86$.

\subsection*{Solution (d)}
\newcommand\SL{\ensuremath{\sqrt{\log{n}}}}

There are at most $n$ paths of length $\SL$ in the cycle (there's one starting
at each vertex). Let $C$ be the sufficiently large constant we got in
\textbf{Solution (c)}, and denote the path from node $i$ to $i + \SL$ as
$P_{i}$. By the union bound, the probability that any of these paths is
unsegmented (i.e, has an active vertex yet) after $C\SL$ rounds is:

\begin{gather*}
  \Prob{\text{any path } P_{i} \text{ has all active nodes}} \\ 
  \leq \Prob{P_{1} \text{ has all active nodes}} + ... + \Prob{P_{n} \text{ has all active nodes}} \\
  \leq \frac{n}{n^{2}} \\
  = \frac{1}{n}
\end{gather*}

Therefore, all paths of active nodes in the cycle at this point are of length no
greater than $\sqrt{\log{n}}$ w.h.p. This means we may run even the
deterministic distributed MIS algorithm on each path, as the deterministic
algorithm will halt on a path of length $\sqrt{\log{n}}$ in $O(\sqrt{\log{n}})$
steps.

\paragraph{The algorithm.} Run Luby's MIS algorithm for $C\SL$
steps on each vertex. Then, switch each vertex to the deterministic algorithm. 

\paragraph{Running time.} With high probability, this will terminate in
$C\SL + O(\SL)$ steps, or $O(\SL)$ steps.

%%Running
%%this algorithm on each distinct path individually
%%
%%Within a cycle, if a vertex $v$ has its degree drop to 1, then it should join
%%the MIS, or its remaining neighbor should. We can modify Luby's algorithm to
%%incorporate this:
%%
%%\begin{codef}
%%status(v) = undecided
%%marked(v) = no
%%while status(v) = undecided do
%%  if deg(v) = 0 then
%%    status(v) = yes
%%  else
%%    marked(v) = 
%%      with probability 1/(deg(v)) -> yes
%%      otherwise -> no
%%ROUND 1
%%  if marked(v) then
%%     // v notifies neighbors and sends its current degree
%%     v.broadcast(deg(v), v.id)
%%  if v receives message from marked neighbor 
%%     of higher degree (or = degr but higher ID) then
%%    marked(v) = no
%%  if marked(v) then
%%    status(v) = yes
%%ROUND 2
%%  // v notifies all neighbors of its status
%%  v.broadcast(status(v))
%%  if v receives a message from a neighbor that is in MIS then
%%    status(v) = no
%%\end{codef}
%%
%%All we have done is changed the probability that $v$ marks itself from
%%$\frac{1}{2 * \Deg{v}}$ to $\frac{1}{\Deg{v}}$. This means vertices with only
%%one neighbor will always mark themselves. In other words: if you have
%%degree 1, you will either be joining (or your neighbor will be joining) the MIS
%%next round. Consider each case:
%%
%%\begin{itemize}
%%\item $v$ has degree 1 and its neighbor has degree 2. Its neighbor joins the
%%  MIS. $v$ sets its status to \lstinline{no}.
%%\item $v$ has degree 1 and its neighbor has degree 1. If $v$ wins the tiebreak,
%%  $v$ joins the MIS; if the neighbor wins the tie break, $v$ becomes inactive.
%%\end{itemize}
%%
%%Consequently, we only need to worry about how many rounds it takes for (w.h.p.)
%%all nodes to have degree $\leq 1$. The algorithm will terminate one round after that.
%%
%% \paragraph{This algorithm terminates in $O(\sqrt{\log{n}})$ rounds with high
%%   probability.}
%% The (easy) analysis of Luby's algorithm shows it halts in
%% $O(\log{n} \cdot \log{\Delta})$ rounds, with $\Delta$ equal to the maximum
%% degree. This is because, with high probability, it takes at most $O(\log{n})$ rounds for
%% all degrees to out of the range $[\Delta / 2, \Delta]$. Our $\Delta$ is 2, so it
%% follows that Luby's algorithm halts in $O(\log{n})$ rounds w.h.p. on cycles of
%% length $n$. So we only need to show that, for our modified Luby's algorithm, it
%% takes $O(\sqrt{\log{n}})$ rounds for all degrees to drop from 2 to 1.
%% 
%% \paragraph{The algorithm is correct.}


\section*{Exercise 2}

A \emph{graph coloring} is an assignment of colors to vertices of a graph so
that no two adjacent vertices have the same color.

Here is a simple ``Luby-like'' randomized, distributed algorithm to color the
vertices of a graph $G = (V, E)$ using $3\Delta$ colors, where $\Delta$ is the
maximum degree of a vertex in $G$. Initially, let
$C_{v} = \{1, 2,...,3 \cdot \Delta\}$ be the set of colors available for each
vertex $v$. Every vertex is initially colorless. In each iteration, each
colorless vertex $v$ in $G$ picks a color from $C_{v}$ uniformly at random. Each
vertex $v$ that has just picked a color then checks if any neighbor has chosen
the same color. If there is a neighbor who has chosen the same color as $v$,
then $v$ ``backs off'' i.e., reverts back to being colorless. Otherwise,
$v$ makes its color choice permanent. Finally, for each colorless $v$, we delete
from $C_{v}$ all colors that became permanent at a neighbor of $v$. 

\begin{enumerate}[label=(\alph*)]
\item Show that in any iteration, the probability that a colorless vertex $v$
  gets colored in that iteration is at least a constant.
\item Let $x$ denote the number of colorless vertices at the start of an
  iteration. Using the concepts of indicator random variables and linearity of
  expectation, show that, in expectation, at most $x/c$ vertices, for constant $c > 1$,
  of the graph remain colorless after the iteration.
\end{enumerate}

\subsection*{Solution (a)}
Suppose, in an arbitrary iteration $i$, that $v \in V$ has $d$ neighbors and $n$
colors to choose from. Note that $2\Delta \leq n \leq 3\Delta$. WLOG, assume neighbors $q_{1}$ through $q_{d}$ still
stand.

\begin{gather*}
  \Prob{v \text{ picks same color as any neighbor}} \\
  \leq \Prob{v \text{ picks same color as } q_{1}} + \\
  ... + \Prob{v \text{ picks same color as } q_{d}} \\
  \leq \frac{d}{n} \\
  \leq \frac{\Delta}{2\Delta} \\
  = \frac{1}{2}
\end{gather*}

So $\Prob{v \text{ becomes colored at iteration } i} \geq \frac{1}{2}$.

\subsection*{Solution (b)}

Assume we're at an arbitrary iteration. Let $V$ be the sum of the indicator
variables $V_{i}$ where

\begin{align*}
  V_{i} =
  \begin{cases}
    &1 \quad \text{vertex } v_{i} \text{ remains colorless in the next iteration} \\
    &0 \quad \text{otherwise}
  \end{cases}
\end{align*}

WLOG, assume vertices $v_{1}$ through $v_{x}$ are those that remain
uncolored. Clearly, $E[V_{i}] \geq \frac{1}{2}$, so

\begin{gather*}
  E[V] \geq E[V_{1}] + E[V_{2}] + ... + E[V_{x}] \\
  = \frac{x}{2}
\end{gather*}

\begin{gather*}
\end{gather*}



\newcommand\DiskCover{\lstinline{DiskCover}}
\newcommand\SetCover{\lstinline{SetCover}}
\newcommand\kCenter{$k$-\lstinline{Center}}

\section*{Exercise 3}
The input containts $n$ points $\varsequence{p}{1}{,}{n}$ in the plane, where each
point $p_{i}$ is specified by its coordinate $(x_{i}, y_{i})$. Additionally, the
input contains $m$ disks $\varsequence{D}{1}{,}{m}$. The disks satisfy two properties:
(i) they all have the same radius $r$, and (ii) the centers of any two disks are
at least $r$ units away from each other. You can assume that the radius $r$ is
some constant and that each disk $D_{j}$ is specified in the input by its
coordinates. Finally, the points and disks together satisfy the property that
every point is contained in some disk.

The problem -- let us call it \DiskCover{} -- is to find the fewest disks to cover
all the points. More formally, we want to find the smallest set
$C \subseteq \{1,2,...,m\}$ such that $p_{i} \in \cup_{j \in C} D_{j}$ for all
$1 \leq i \leq n$.

Design a constant-factor approximation algorithm for the \DiskCover{} problem. Your
answer should contain two parts:

\begin{enumerate}[label=(\alph*)]
\item an algorithm description
\item analysis showiing that the algorithm is a constant approximation of \DiskCover.
\end{enumerate}

\textbf{Note:} the specific constant that you get for the approximation is not
important, as long as you get a constant.

\textbf{Hint:} This is a version of \SetCover. Think about the different \SetCover{}
approximation algorithms we have discussed in class and use an algorithm that is
likely to yield a constant-approximation.

\subsection*{Solution (a)}

We simply use the primal-dual approximation to \SetCover, which gives an
$f$-approximation. We will show in \textbf{Solution (b)} that, for \DiskCover, $f$ is bounded above
by 9, giving a constant approximation.

\paragraph{Reducing \DiskCover{} to \SetCover.} For a given disk $d$ and radius $r$,
create set

\[
  S_{d} = \{\, p\; \mid\; D(d, p) \leq r \,\}
\]

and let the ground set of elements $E$ simply be the set of points given. Assuming
a constant time to calculate distances, this is computable in $O(n * m)$ time ($n$ being the number of
points, $m$ the number of disks).

\paragraph{Recovering a solution to \DiskCover{} from \SetCover.} Simply
align the sets chosen $\varsequence{S}{j}{,}{k}$ with disks $\varsequence{D}{j}{,}{k}$.


\newcommand\Degr{\ensuremath{^{\circ}}}
\subsection*{Solution (b)}
\begin{lemma}
  for an arbitrary point $p$ covered by $f$ disks, $\varsequence{D}{1}{,}{f}$,
  and their centers $\varsequence{d}{1}{,}{f}$, the angle at point $p$
   is at least 60\Degr{} for $1 \leq i, j \leq f$,
  $i \neq j$.
\end{lemma}

\begin{proof}
  Let point $p$ form a triangle with arbitrary centers $d_{i}$ and
  $d_{j}$. $p$ is covered by disks $D_{i}$ and $D_{j}$, meaning
  $D(d_{i}, p) \leq r$ and $D(p, d_{j}) \leq r$. However, by assumption,
  \[
    D(d_{i}, d_{j}) \geq r
  \]

  So, this is the longest edge in the triangle and therefore sits opposite the
  greatest angle (at $p$). The greatest angle must be at least 60 degrees.
\end{proof}

Now let point $p$ be arbitrary and suppose $p$ is covered by disks
$\varsequence{D}{1}{,}{f}$.  Let's create a new disk $D_{p}$ with center
$p$. Split this disk into eight 45\Degr{} octants and consider arbitrary disk
centers $d_{i}$ and $d_{j}$. The angle at $p$ with respect to the triangle
formed by these three points is 60\Degr, which exceeds the angle of a given
octant.  So we may conclude that there may not be more than one disk in each
octant: if there were, the angle at $p$ would have to be less than
60\Degr. Thus, $p$ is covered by at most $8$ other disks and (possibly) itself
(if there is a disk center exactly at $p$). If the optimum is for point $p$ to
be covered just by one disk and we give you $9$, then this is a
$9$-approximation.


\section*{Exercise 4}

Suppose (for the moment) that we somehow know the radius $\rho$ of an optimal
solution to the \kCenter{} problem. Given this optimal radius $\rho$, we can
model the problem of finding $k$ centers that yield this radius as the following
integer program (IP). Recall that we are given $n$ points
$P = \{\varsequence{p}{1}{,}{n} \}$ and $D : P \times P \to R_{\geq 0}$ is a metric on $P$.

\begin{align*}
    \sum_{i = 1}^{n} x_{i} &= k \\
    \sum_{i : D(p_{i}, p_{j}) \leq \rho} x_{i} &\geq 1 \quad \text{for each } j = 1,2,...,n \\
    x_{i} &\in \{0, 1\} \quad \text{for each } i = 1,2,...,n \\
\end{align*}

Here $x_{i} \in \{0, 1\}$, $1 \leq i \leq n$ is a choice variable indicating the
selection of $p_{i}$ as a center. Do not be thrown off by the fact that this integer program just hast
constraints and no objective function; it just means that any feasible solution, i.e, a solution satisfying
these constraints, is good enough.

\begin{enumerate}[label=(\alph*)]
\item Explain, in 2-3 sentences, how this integer program models the problem of
  finding $k$ centers that realize the given radius $\rho$.
\item Since we cannot solve this integer program in polynomial time, we replace the $n$ integrality constraints $x_{i} \in \{0, 1\}$ by non-negativity constraints $x_{i} \geq 0$ and solve this
  LP relaxation. let $x_{i}^{*}$ denote the obtained solution to the LP relaxation.

  Here is your task: Design a \emph{deterministic} algorithm for rounding the
  $x_{i}^{*}$ values so that the rounded values, which we will denote by $z_{i} \in \{0, 1\}$,
  satisfy the folowing constraints:
  \begin{align*}
    \sum_{i = 1}^{n} z_{i} &= k \\
     \sum_{i : D(p_{i}, p_{j}) \leq 2\rho} z_{i} &\geq 1  \quad \text{for each } j = 1,2,...,n \\
    z_{i} &\in \{0, 1\} \quad \text{for each } i = 1,2,...,n \\
  \end{align*}
  Note the use of ``$2\rho$'' in the second constraint above. This means that
  once we round the $x_{i}^{*}$'s to $z_{i}^{*}$'s, we are allowing ourselves
  twice the radius to cover all points in the input. So this rounding algorithm gives us a set
  of $k$ centers that form a 2-approximation to the \kCenter{} problem.

  Here are some sugestions for how to think about a rounding algorithm. Pick a
  point $p_{i}$ with $0 < x_{i}^{*} < 1$. For example, $x_{i}^{*}$ may be $0.3$
  telling us that $p_{i}$ has been fractionally chosen to be a center. Consider
  all points $p_{j} \neq p_{i}$ such that $D(p_{i}, p_{j}) \leq \rho$. For each
  such point $p_{j}$, add $x_{j}^{*}$ to $x_{i}^{*}$ and then round down
  $x_{j}^{*}$ to 0. In other words, we ``transfer'' $x_{j}^{*}$ from $p_{j}$ to
  $p_{i}$. Now think about the following two questions: (i) what is the value of
  $x_{i}^{*}$ now? (ii) What should we do with all the points that $p_{j}$ was
  helping cover as a ``fractional'' center?
\item Finally, explain in 2-3 sentences how to get rid of the assumption that the optimal
  radius $\rho$ is given to us. in other words, describve a polynomial time algorithm
  that allows us to generate all candidate values for the optimal radius $\rho$ and consider
  each value one-by-one.
\end{enumerate}

\subsection*{Solution (a)}
This IP will return exactly $k$ $x_{i}$'s, as per the first constraint. Each
$x_{i}$ represents a center, so that's $k$ centers \checkmark. The second
constraint guarantees two things: When $i \neq j$, we guarantee that at least
one of the points within $\rho$ from you is a center. Now suppose that you're
further than $\rho$ away from all other points. Then, when $i = j$, the trick is
that $D(p_{i}, p_{j}) = 0 \leq \rho$, which means you have to become a center if you're a
loner.

\subsection*{Solution (b)}

Our algorithm is more or less what's in the problem description, except we
assign simply 1 to a center rather than adding all of its neighbors to it.

\newcommand\xstar[1]{\ensuremath{x^{*}_{#1}}}
\newcommand\zstar[1]{\ensuremath{z^{*}_{#1}}}

\begin{codef}
def Rounding($\rho$, D, k, $\xstar{}$):
  for i in [1 .. n]:
    if $\xstar{i}$ > 0:
      S $\gets$ [ all neighbors of $\xstar{i}$ ]
      if sum S + $\xstar{i}$ $\geq$ 1:
        $\xstar{i}$ $\gets$ 1
        set all $\xstar{j}$ in S to 0
  for any $\xstar{i}$ remaining s.t. $0 < \xstar{i} < 1$:
     $\xstar{i}$ $\gets$ 0
  if sum $\xstar{}$ < k:
    arbitrarily set (k - sum $\xstar{}$) $\xstar{}$'s to 1
  return $\xstar{}$
\end{codef}

We now argue for correctness with respect to the integer program above. Denote
the output of \lstinline{Rounding} as $\zstar{}$.

\paragraph{The algorithm returns exactly k centers.} We have to argue
that $\sum_{i} \zstar{i} = k$. We'll simply argue that it's not the case that
$\sum_{i} \zstar{i} < k$ or $\sum_{i} \zstar{i} > k$.

For the first case, we have a guard that protects us: if
$\sum_{i} \xstar{i} < k$ then we add just enough centers so that
$\sum_i \xstar{i} = k$. For the second case, our modifications to $\xstar{}$ are
strictly decreasing: if we set $\xstar{i} \gets 1$, it is always case that the
net change to $\sum_{j} \xstar{j}$ is negative, as we confirmed \lstinline{Sum S +}
 $\xstar{i} \geq 1$.

 \paragraph{All points in $\zstar{}$ are within $2\rho$ of a center.}

%% Recall
%% that $\xstar{}$ begins having met this criteria:
%% \[
%% \sum_{i : D(p_{i}, p_{j}) \leq \rho} x_{i} \geq 1 \quad \text{for each } j = 1,2,...,n 
%% \]

Suppose we are to set $\xstar{i} \gets 1$ and $\xstar{j} \gets 0$, where
$D(\xstar{i}, \xstar{j}) \leq \rho$. Let $p$ be some point within $\rho$ distance of $\xstar{j}$ but not
$\xstar{i}$. By the triangular inequality,

\[
D(x_{i}, p) \leq D(x_{i}, x_{j}) + D(x_{j}, p) \leq \rho +\rho = 2\rho
\]

So $p$ is within $2\rho$ of $\xstar{i}$, as desired.

\paragraph{Integrality.} We iterate over every point $\xstar{i}$. Denote this
point's ``$\rho$-neighborhood'' as $S$. Suppose $\sum S + \xstar{i} \geq 1$. This will happen at
the start of the algorithm, or if $\xstar{i}$ and all points in $S$ are yet
unmodified. In this case, $\xstar{i}$ is set to 1 and all points in $S$ are set to
0. So they're integers.

Now suppose $\sum S + \xstar{i} < 1$. This will only happen if a $\rho$-neighbor of
$\xstar{i}$ is reduced to $0$. If this is the case, then we have shown above
that $p$ is still covered. So we are justified in setting $p \gets 0$ at the end
of the algorithm.

\paragraph{This algorithm runs in polynomial time.} Within a \lstinline{for} loop,
it performs two linear operations (filtering $\xstar{}$ and mapping over
\lstinline{S}). So it's quadratic.
%% \begin{codef}
%% def Rounding($\rho$, D, k, x*):
%%   for i from 1 to n do:
%%     if x*[i] > 0:
%%       S $\gets$ { j $\mid$ D(p_i, p_j) $\leq$ $\rho$ }
%%       if ($\sum_{j \ in S}$ j $\geq$ 1:
%%         x*[i] $\gets$ 1
%%         Set all x*[j] = 0 where j $\in$ S
%%   endfor
%%   randomly pick (k - $\sum_{i}x*[i]$) centers
%% \end{codef}

\newcommand\Gonz{\lstinline{GonzalezKCenter}}
\subsection*{Solution (c)}

The \Gonz{} algorithm (see Erickson) actually computes an approximation to the
optimal radius (call this approximation $\rho'$) in polynomial time such that
$OPT \leq \rho' \leq 2 \cdot OPT$, which implies

\[
\frac{1}{2} \rho' \leq OPT \leq \rho'
\]

So to generate candidates, we can look at each pair combo $(p_{i}, p_{j})$
such that

\[
\frac{1}{2}\rho' \leq D(p_{i}, p_{j}) \leq \rho'
\]

Any such distance is a candidate.

\newcommand\BalancedSum{\lstinline{BalancedSum}}
\section*{Exercise 5}

Consider the following optimization problem, let us call it \BalancedSum. Given a set $X$
of positive integers, our task is to partition $x$ into disjoin subsets $A$ and $B$ such that

\[
\text{max} \{ \sum_{x \in A} x, \sum_{y \in B} y \}
\]

is as small as possible. In other words, we want to partition $X$ into two subsets
$A$ and $B$ such that the sums of the elements in the two sets are as balanced, i.e,
close to each other, as possible.

\newcommand\from{\leftarrow}
\newcommand\GreedyPartition{\lstinline{GreedyPartition}}

\begin{enumerate}[label=(\alph*)]
\item Prove that the following algorithm yields a $3/2$-approximation to \BalancedSum.

  \begin{figure}[H]
    \centering
\begin{codef}
GreedyPartition(X[1..n]):
  a $\from$ 0
  b $\from$ 0
  for i $\from$ 1 to n
    if a < b then
      a $\from$ a + X[i]
    else
      b $\from$ b + X[i]
  return max{a, b}
\end{codef}
\end{figure}

\item Give an example of an array $X$ for which the cost of the output of \GreedyPartition{}
  is 50\% larger than the cost of the optimal partition.
\item Prove that if the array $X$ is sorted in non-increasing order, then \GreedyPartition{}
  achieves the approximation factor of 4/3.
\end{enumerate}

\newcommand\OPT{\ensuremath{OPT}}
\subsection*{Solution (a)}

Suppose the input array $X$ is split into partitions $A$ and $B$. The best possible scenario
of any array $X$ is that the input is split evenly, i.e,

\[
  \sum_{i} A[i] = \sum_{i} B[i] = \frac{1}{2}\sum_{i} X[i]
\]

So \OPT{} should be no less than this value.

\begin{equation}
\OPT \geq \frac{1}{2}\sum_{i} X[i]
\end{equation}

Let $s_{a}$ and $s_{b}$ denote the sums of $A$ and $B$, respectively. WLOG,
assume $s_{a} \geq s_{b}$. $A$ and $B$ partition $X$, so

\begin{equation}
  s_{a} + s_{b} = \sum_{i} X[i]
\end{equation}

Further, the algorithm entails that

\begin{equation}
  0 \leq s_{a} - s_{b} \leq \max X
\end{equation}

Equation (2) is obvious. To prove (3): there will come a time at which point we
quit adding elements into $A$. This will either happen at the very end (the last
element of $X$), or before it. Suppose it happens at the
very end.  Then $s_{a} = s_{a}' + X[n]$, where $s_{a}'$ denotes the
sum of $A$ before the addition of this last element. For $X[n]$ to be
added to $s_{a}'$, it must be the case that

\begin{align*}
  \phantom{} &s_{a}' < s_{b} \\
  \Rightarrow &s_{a}' - s_{b} < 0
\end{align*}

Adding $X[n]$ to both sides,

\begin{align*}
  \phantom{} &s_{a}' + X[n] - s_{b} < X[n] \\
  \Rightarrow &s_{a} - s_{b} < X[n]
\end{align*}

and of course, $X[n] \leq \max X$, so

\begin{align*}
  s_{a} - s_{b} \leq \max X
\end{align*}


Now, instead suppose we add $X[j]$ to $A$ and add all $X[j + 1], ..., X[n]$ to $B$. Let
$s_{a}'$ and $s_{b}'$ denote the sums of $A$ and $B$ before adding $X[j]$ to
$A$. Then $s_{b} = s_{b}' + X[j + 1] + ... + X[n]$, and

\begin{align*}
  \phantom{}  &s_{a}' - s_{b}' + X[j] \leq X[j] \leq \max X \\
  \Rightarrow &s_{a} - (s_{b}' + X[j + 1] + ... + X[n]) \leq X[j] - X[j + 1] - ... - X[n] \\
  \leq &\max X - X[j + 1] - ... - X[n] \leq \max X \\
  \Rightarrow &s_{a} - s_{b} \leq \max X - X[j + 1] - ... - X[n] \leq \max X \\
\end{align*}

If we add equations (2) and (3) together, we get:

\begin{align*}
              &s_{a} + s_{b} + (s_{a} - s_{b}) \leq \sum_{i} X[i] + \max X \\
  \Rightarrow &2s_{a} \leq \sum_{i} X[i] + \max X \\
  \Rightarrow &s_{a} \leq \frac{1}{2} \sum_{i} X[i] + \frac{1}{2} \max X
\end{align*}

Of course, we know $OPT \geq 1/2 \cdot \sum_{i} X[i]$, so

\[
s_{a} \leq OPT + \frac{1}{2} \max X
\]

Now we split on two scenarios. First, suppose that
$\max X \leq 1/2 \cdot \sum X[i]$. As

\begin{align*}
  \phantom{} &\max X \leq \frac{1}{2} \cdot \sum X[i] \leq OPT \\
  \Rightarrow &\frac{1}{2} \max X \leq \frac{1}{2} OPT
\end{align*}

it follows that

\[
s_a \leq OPT + \frac{1}{2} OPT = \frac{3}{2} OPT
\]

Alternatively, suppose $\max X > 1/2 \cdot \sum X[i]$. We now claim that if this is the
case, $\max X = OPT$. For ease of notation (and WLOG), let's denote $X$ as the
sequence $\varsequence{x}{1}{,}{n}$, and $\max X$ as $x_{1}$. Now observe

\begin{align*}
  x_{1} &\geq \frac{1}{2} (\varsequence{x}{1}{+}{n}) \\
  \Rightarrow 2x_{1} &\geq \varsequence{x}{1}{+}{n} \\
  \Rightarrow x_{1} &\geq \varsequence{x}{2}{+}{n}
\end{align*}

We have to put $x_{1}$ somewhere, so an optimal partition would be:

\begin{align*}
  A &= x_{1} \\
  B &= x_{2},...,x_{n}
\end{align*}

In this case, $s_{a} \geq s_{b}$, and adding any more elements into $A$ would result
in a less optimal solution. Therefore, in this arrangement, $s_{a} = \max x$ is optimal.

Finally, if $\max x = OPT$ then 

\begin{align*}
  \phantom{} &s_{a} \leq OPT + \frac{1}{2} \max X \\
  &s_a \leq OPT + \frac{1}{2} OPT = \frac{3}{2} OPT
\end{align*}

and the algorithm is a $3/2$-approximation.


\subsection*{Solution (b)}
The array \lstinline{[1, 1, 2]} produces a partition of $(1, 3)$ instead of the
optimal $(2, 2)$.


\subsection*{Solution (c)}
We will formalize exactly as we did in (a): We partition the sequence
$X = \varsequence{x}{1}{,}{n}$ into arrays $A$ and $B$, let $A$ be the
larger of the two WLOG, and denote the sums $\sum_{i} A[i]$ and $\sum_{i} B[i]$
as $s_{a}$ and $s_{b}$, respectively. We also will intermittently switch between
denoting $X$ as a sequence and as an array, without warning.


We think again about the last element to be put into $A$. Suppose it's $X[j]$
with $2 < j \leq n$, and let $s_{a}'$ denote the size of $a$ before $x_{j}$ is
added. Then

\begin{align*}
  s_{a}' &< s_{b}' \\
  \Rightarrow s_{a}' + x_{j} - s_{b}' &< x_{j} \\
  \Rightarrow s_{a} - s_{b}' &< x_{j}
\end{align*}

We specified that $j > 2$, i.e, we know for certain $A$ and $B$ are nonempty. If
$B$ were empty, we'd have placed $x_{j}$ in it. Further, $x_{i} \geq x_{j}$ for
$1 \leq i \leq j$, and there is some $x_{i}$ in that range inside $B$. So we
conclude

\begin{align*}
  s_{a} - s_{b}' &< x_{j} \leq s_{b} \\
\end{align*}

and $s_{a} - s_{b} \leq s_{a} - s_{b}'$, so
\begin{align*}
  s_{a} - s_{b} &< s_{b} \\
  \Rightarrow s_{a} &< 2 s_{b}
\end{align*}

Now, as $s_{a} + s_{b} = \sum_{i} X[i]$,

\begin{align*}
  \phantom{}& s_{a} + s_{b} < 3 s_{b} \\
  \Rightarrow&  \sum_{i} X[i] < 3 s_{b} \\
  \Rightarrow&  \sum_{i} X[i] < 3 (\sum_{i} X[i] - s_{a}) \\
  \Rightarrow&  - 2 \sum_{i} X[i] < - 3 s_{a} \\
  \Rightarrow& s_{a} \leq \frac{2}{3} \sum_{i} X[i]
\end{align*}

And it follows that


\begin{align*}
  s_{a} \leq \frac{2}{3} \cdot \sum_{i=1}^{n}X[i] \leq \frac{4}{3} \cdot \frac{1}{2} \sum_{i=1}^{n}X[i] \leq \frac{4}{3} OPT
\end{align*}

For the final case, suppose we place a big boy into $A$ right at the start, i.e,
$X[j]$ is the last element placed into $A$ and $j \in \{1, 2\}$. Then, by the same
reasoning as before, $X[j] = OPT$. Recall:

\begin{align*}
  A &= x_{j} \\
  B &= x_{j + 1},...,x_{n}
\end{align*}

So we cannot rearrange these buckets in a manner that reduces $\text{max} \{ \sum_{x \in A} x, \sum_{y \in B} y \}$.


\end{document}
