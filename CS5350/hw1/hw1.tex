\documentclass[11pt, a4paper]{article}


\usepackage{url}
% DOCUMENT LAYOUT
\usepackage[margin=1in]{geometry}
\parindent=15pt

% Reduce spacing in enumerate, enumerate and titles
\usepackage{multicol}
\usepackage{enumitem}
\setenumerate{nolistsep}
\setenumerate{nolistsep}
% \setlength{\parskip}{1em}

% FONTS
\usepackage[utf8]{inputenc}

% HEADINGS and TITLES
\usepackage{sectsty}
\usepackage[normalem]{ulem}

% packages
\usepackage{amsmath, amsfonts, latexsym, listings, cite, amssymb,
  amsthm, stmaryrd, scrextend, float, mathtools}

\usepackage{graphicx}
\graphicspath{ {./img/} }

% Not native to tex
\usepackage{bussproofs}
\usepackage{qtree}
\usepackage{tikz-cd}

\usepackage{titling}
\setlength{\droptitle}{-90pt}
\title{CS5350: Homework 1}
\author{Alex Hubers, Jamil Gafur}

% Thm styling if I need it
\theoremstyle{definition}
\newtheorem{definition}{Definition}[section]
\newtheorem{theorem}{Theorem}[section]
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem*{remark}{Remark}

% pretty references
\usepackage{prettyref}
\newcommand{\pref}[1]{\prettyref{#1}}
\newrefformat{assn}{Assumption~\ref{#1}}
\newrefformat{prop}{Property~\ref{#1}}
\newrefformat{fig}{Figure~\ref{#1}}
\newrefformat{sec}{Section~\ref{#1}}
\newrefformat{thm}{Theorem~\ref{#1}}
\newrefformat{lem}{Lemma~\ref{#1}}
\newcommand\secref[1]{\pref{sec:#1}}
\newcommand\figref[1]{\pref{fig:#1}}
\newcommand\appref[1]{\pref{app:#1}}

\bibliographystyle{acm}

\usepackage{listings}
\lstloadlanguages{Haskell}
% \usepackage{libertine}
\usepackage{lmodern}
\lstset{
  showstringspaces=false,
  language=Haskell,
  xleftmargin=\parindent,
  basicstyle=\ttfamily,
  % keepspaces=false,
  morecomment=[l]{--},
  mathescape=true,
  numberstyle=\tiny,
  flexiblecolumns=false,
  basewidth={0.5em,0.45em},
  morekeywords={instance, Theorem, Lemma, class, where, if, data, then, else, type, case, of, require, newtype},
  keywordstyle={\bfseries},%\underbar,
}
\lstnewenvironment{code}{}{}
%\lstMakeShortInline{|}
\lstnewenvironment{codef}{\lstset{basicstyle=\ttfamily\small,xleftmargin=1.5em}}{}

% My Commands
\newcommand{\SetOf}[1]{\ensuremath{\{#1\}}}
\newcommand{\banana}[1]{\ensuremath{\llparenthesis\, #1\, \rrparenthesis}}

\newcommand{\Start}{\ensuremath{\vartriangleright}}
\newcommand{\Halt}{\ensuremath{\square}}

\newcommand{\qstate}[1]{\ensuremath{q_{#1}}}
\newcommand{\qcheck}[1]{\qstate{check_{#1}}}

\newcommand{\qstart}{\qstate{start}}
\newcommand{\qhalt}{\qstate{halt}}
\newcommand{\qscan}{\qstate{scan}}

\newcommand{\LA}{\ensuremath{\langle}}
\newcommand{\RA}{\ensuremath{\rangle}}
\newcommand{\binset}{\ensuremath{\lbrace 0, 1 \rbrace}}

% BEGIN DOCUMENT
\begin{document}
\maketitle


\section{Exercise 1}

Consider the following \emph{two-source maximum flow} problem, which we will
call 2Source-MaxFlow. You ar given a directed graph $G = (V, E)$ and three
special vertices $s_{1}, s_{2}, t \in V$, with $s_{1}$ and $s_{2}$ being two
source vertices and $t$ the sink. The value of the flow is the net outflow at
$s_{1}$ plus the net outflow of at $s_{2}$. All definitions otherwise remain the
same as that of MaxFlow.

Describe a polynomial time algorithm for this problem,
by efficiently reducing it to MaxFlow. That is,
given input $G = \LA (V, E), s_{1}, s_{2}, t, c \RA$,
(a) show that it can be modified in polynomial time to
an input $G' = \LA (V', E'), s', t', c' \RA$ and (b) from
the solution to MaxFlow on input $G'$, we can extract
(in polynomial time) a solution to 2SourceMaxFlow on
$G$.

\subsection{Solution}

Let $G$ be defined as above. We will first describe
a polynomial time transformation from $G$ to $G'$. Then
we will show that a max flow $f'$ obtained from $G'$
can be polynomially translated to a max flow $f$ for
$G$.

\paragraph{The Transformation.}
The intuition is that we can prefix $s_{1}$ and $s_{2}$ with new node $s'$ that is
directly before and shares one directed edge into each $s_{1}$ and
$s_{2}$. Precisely, add $s'$ to $V$ and edges $s' \to s_{1}$, $s' \to s_{2}$
such that $c'(s' \to s_{n}) = \sum_{v \in V} c(s_{n} \to v)$ where
$n \in \binset$ and $c(s_{n} \to v) = 0$ if there is no edge $s_{n} \to v$.
\begin{center}
\includegraphics[scale=0.8]{1a.png}
\end{center}


\paragraph{polynomial time.}
This transformation is in polynomial time. We are merely:
adding a vertex to $V$ (either constant or linear, depending
on the data structure); adding two edges to $E$ (constant or linear);
and performing two summations over $c$ (linear).

\paragraph{Recovery of solution.}
Given $G'$ we receive flow $f' : E' \to \mathbb{R}^{+}$ from MaxFlow.  We can let
$f = f'$ except with $f(s' \to s_{1}) = f(s' \to s_{2}) = 0$ (assuming that $f'$
is a lists of ordered pairs, this is a linear operation). We now argue that $f$ is
a maximum flow for $G$. Firstly, $f$ satisfies feasability and flow conservation
trivially. All edges are feasible because $E \subseteq E'$ and all $e' \in E'$
are feasible with respect to $c'$, and $\forall e \in E, c(e) = c'(e)$. Flow
conservation follows similarly: $V \subseteq V'$ and all $v$ obey flow
conservation with respect to $f'$. Intuitively -- all we did was cut off edges
$s' \to s_{1}$ and $s' \to s_{2}$, so conservation and feasability carry over
for the rest of the edges and vertices.

\paragraph{Correctness.}
For sake of contradiction, suppose $f$ is not maximal. Then there exists some
augmenting path starting at $s_{1}$ or $s_{2}$ through which we can push at
least one additional unit of flow. WLOG, suppose it is $s_{1}$. If this is the
case, then there exists an edge $s_{1} \to v$ unsaturated, implying the edge
$s' \to s_{1}$ under $f'$ is not saturated. This follows from $c'(s' \to s_{1})$ being
the sum of the outbound capacities of $s_{1}$. Thus we can source an additional
unit of flow from $s'$, making the augmenting path in $G$ an augmenting
path in $G'$, contradicting the maximality of $f'$.

%% Jamil picture here

\section{Exercise 2}
The input is a directed graph $G = (V, E)$, special vertices $s, t \in V$, and two
capacity functions $c_{1}, c_{2} : E \to R^{+}$. A flow $f : E \to \mathbb{R}$
is said to be feasible for the input if
$c_{1}(u \to v) \leq f(u \to v) \leq c_{2}(u \to v)$ for all edges $u \to v \in E$.
\noindent
Show that there is a polynomial time algorithm for determining if there is a feasible flow
for the input.

\subsection{Solution}
This can be formulated as a linear programming problem (and is therefore
polynomial). Further, we can find the maximum feasible flow (if one exists). The
objective function can be

$$
\text{max}(\sum_{w} f(s \to w) - \sum_{u} f(u \to s))
$$

\noindent
and the constraints can be

\begin{align*}
&\partial f(v) = 0, &\forall\, v \in (V - \{s, t\}) & \quad\text{\emph{conservation.}} \\
&f(u \to v) \leq c_{2}(u \to v),\; &\forall\, (u \to v) \in E & \quad\text{\emph{feasibility 1.}} \\
&f(u \to v) \geq c_{1}(u \to v),\; &\forall\, (u \to v) \in E & \quad\text{\emph{feasibility 2.}} \\
&f(u \to v) \geq 0,\; &\forall\, (u \to v) \in E & \quad\text{\emph{default lower bound.}} \\
\end{align*}

A few notes:
\begin{itemize}
\item We can also formulate this problem by letting the flow assigned to each
  edge $e \in E$ be denoted by some $x_{n}$ variable, as was done in class.
\item The \emph{default lower bound} is actually implied by \emph{feasability
    2}, and is stated for consistency but could be omitted.
\item The \emph{feasability 2} constraint leaves this system not in normal form,
  but it can be transformed into normal form by a simple negation:
  $-f(u \to v) \leq -c_{1}(u \to v),\; \forall\, (u \to v) \in E$. The equality in \emph{conservation}
  can also be transformed.
\end{itemize}

\section{Exercise 3}
The Environmental Sciences (ES) department at the University is conducting a
large-scale atmospheric measurement experiment. The goal is to get accurate
measurements on a set $S$ of $n$ different atmospheric conditions (e.g., ozone
level, temperature, Carbon dioxide level, etc.). The ES department has $m$
different balloons that they plan on sending into the atmosphere to make these
measurements. However, because of differences in instruments balloons may have
on board, each balloon $i = 1,2,...,m$ has the ability to measure only a
subset $S_{i} \subseteq S$ of the conditions. Finally, to make the measurements accurate we
want each balloon to measure at most 2 conditions and each condition to be
measured by at least $k$ different balloons.

\begin{itemize}
\item (a) Describe a polynomial-time algorithm that takes as input the $n$
  conditions, the sets $S_{i}$ for each $i = 1, 2,...,m$, and the positive
  integer $k$ and determines whether there is a way to measure each condition by
  at least $k$ different balloons, with each balloon $i$ measuring at most two
  conditions in $S_{i}$.
\item (b) Prove that your algorithm is correct and it runs in polynomial time.
\end{itemize}

\subsection{Solution (a)}

We can encode this as a max-flow problem.

\begin{center}
\includegraphics[scale=0.75]{img/3a.png}
\end{center}


The idea is that we are encoding the semantics of the problem in the capacities
of edges. There are principally three ``semantic'' edge sets:
\begin{itemize}
\item For each balloon and condition subset $S$, we add an edge $s \to S$ representing a balloon
  measuring conditions in $S$. Because each capacity is 2, a balloon
  \textbf{can't} measure more than 2 conditions in $S$.
\item For each condition subset $S$, we add an edge with capacity 1 to all
  $c \in S$. So a saturated edge, for example, from $S \to c$ indicates that $c$
  has been measured once.
\item For each condition $c$, we add an edge $c \to t$ with capacity $k$.
  The flow at $c \to t$ represents how many balloons have measured the condition $c$;
  saturation along this edge indicates it was measured exactly $k$ times, as desired.
\end{itemize}

We say that the problem is satisfied if the maximum flow saturates all
$c \to t$ edges.

\subsection{Solution (b)}
Encoding the problem as a flow network gives correctness and polynomial running
time of the algorithm for free: just choose your favorite Edmonds-Karp heuristic
and find the max flow. The algorithm is otherwise correct by construction; if
there exists a flow such that edges $c \to t$ are saturated, then that means
each condition was measured by $k$ balloons. Note that the maximum flow should
tell us if it is possible to saturate these edges; the value of the flow
is equal to $\partial f(s) = - \partial f(t)$. So a maximization of flow will maximize the
flow at the $c \to t$ edges.


\section{Exercise 4}
Suppose instead of capacities, we consider networks where each edge $u \to v$
has a non-negative \textbf{demand} $d(u \to v)$. Now an $(s, t)$-flow $f$ is \emph{feasible}
iff $f(u \to v) \geq d(u \to v)$ for every edge $u \to v$ (Feasible flow values can now be arbitrarily
large). A natural problem in this setting is to find a feasible $(s, t)$-flow of \emph{minimum} value.

\begin{itemize}
\item (a) Describe an efficient algorithm to compute a feasible $(s, t)$-flow, given the graph,
  the demand function, and the vertices $s$ and $t$ as input.
\item (b) Suppose you have access to a subroutine MaxFlow that computes
  maximum flows in networks with edge capacities. Describe an efficient algorithm
  to compute a minimum flow in a given network with edge demands;
  your algorithm should call MaxFlow exactly once.
\item (c) State an analogue of the max-flow min-cut theorem for this setting.
\end{itemize}

\subsection{Solution (a), (b)}
Like in Exercise 2, this can be formulated as an (minimization) LP problem.  So
a polynomial time solution \textbf{for finding the minimum feasible flow}
follows for free.  The objective function is

$$
\text{min}(\sum_{w} f(s \to w) - \sum_{u} f(u \to s))
$$

\noindent
and the constraints can be

\begin{align*}
&\partial f(v) = 0, &\forall\, v \in (V - \{s, t\}) & \quad\text{\emph{conservation.}} \\
&f(u \to v) \geq d(u \to v),\; &\forall\, (u \to v) \in E & \quad\text{\emph{feasibility.}} \\
&f(u \to v) \geq 0,\; &\forall\, (u \to v) \in E & \quad\text{\emph{default lower bound.}} \\
\end{align*}

\subsection{Solution (c)}
A min-flow's value is equal to the summed demands of the max-cut.

\section{Exercise 5}
A new assistant professor proposes a new algorithm called GreedyFlow:

\begin{codef}
def GreedyFlow(G, c, s, t):
  for every edge e in G:
    f(e) $\leftarrow$ 0

  while $\exists$ path from s to t:
    $\pi$ $\leftarrow$ an arbitrary path from s to t
    $F$ $\leftarrow$ minimum capacity of any edge in $\pi$
    for every edge $e$ in $\pi$:
      f(e) $\leftarrow$ f(e) + F
      if c(e) = F:
        remove e from G
      else:
        c(e) $\leftarrow$ c(e) - F
  return f

\end{codef}

\begin{itemize}
\item (a) Show that GreedyFlow does not always compute a maximum flow.
\item (b) Show that GreedyFlow is not even guaranteed to compute a good
  approximation to the maximum flow. That is, for any constant $\alpha > 1$,
  there is a flow network $G$ such that the value of the maximum flow
  is more than $\alpha$ times the value of the flow computed by the GreedyFlow.
\end{itemize}

\subsection{Solution (a)}
We can use a very simple network to demonstrate that GreedyFlow does not
compute a maximum flow.

\begin{center}
\includegraphics[scale=1.0]{5_a.png}
\end{center}


\noindent
In this example, the max flow has value 2 -- simply send 1 unit of flow along
the $s \to a \to t$ and $s \to b \to t$ paths. However, the greedy algorithm may choose the path
with edge $a \to b$, in which case the edges $s \to a$, $a \to b$, and $b \to t$
are removed, leaving no additional path for the greedy algorithm to choose. So
GreedyFlow here produces a flow with value 1.

\subsection{Solution (b)}
There is a simple extension to the solution above that can produce a network for
which the max flow is $\alpha$ but the greedy algorithm produces a flow with
value 1. Let each network graph be indexed by $\alpha$, e.g, $G_{2}$ is given
above, as the graph above has a max flow that is twice the greedy flow's
value. The pattern for $G_{3}$ is below:

\begin{center}
\includegraphics[scale=0.7]{5b1.png}
\end{center}

\noindent
Each edge above has capacity 1.
Observe that the resulting path partitions the graph into
$S = \{s, a_{2}, a_{3}, b_{3} \}$ and $T = \{ t, a_{1}, b_{1}, b_{2} \}$,
so GreedyFlow gives value 1 whereas the max flow is trivially 3.


More broadly: For each increment to $\alpha$, we add a new ``column'' and
``row'' of vertices.  There is exactly one downward edge in each column --
forming a ``zig-zag''.

\begin{center}
\includegraphics[scale=0.7]{5b2.png}
\end{center}

We let GreedyFlow choose the zig-zag path. At each step, the zig-zag cuts off an
outbound path along a row. So in one pass, the GreedyFlow algorithm effectively
bisects the graph, leaving no additional paths and giving the flow a value of
1. However, the max flow value is trivially $\alpha$, as there are $\alpha$ straight paths
from $s$ to $t$ which MaxFlow would saturate.

\end{document}
